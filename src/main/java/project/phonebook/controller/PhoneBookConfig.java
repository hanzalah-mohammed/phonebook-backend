package project.phonebook.controller;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import project.phonebook.dao.PhoneBookDao;

@Configuration
public class PhoneBookConfig {

    @Bean
    CommandLineRunner commandLineRunner (PhoneBookDao phoneBookDao){
        return args -> phoneBookDao.addSampleEntries();
    }
}
