package project.phonebook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import project.phonebook.model.PhoneBook;
import project.phonebook.service.PhoneBookService;

import java.util.Map;

@RestController
@RequestMapping(path = "phonebook")
public class PhoneBookController {

    @Autowired
    private PhoneBookService phoneBookService;

    @GetMapping()
    public Map<String, String> getAllEntries(){
        return phoneBookService.getAllEntries();
    }

    @PostMapping(path = "add")
    public void addEntry(@RequestBody PhoneBook entry){
        phoneBookService.addEntry(entry);
    }

    @PutMapping(path = "update")
    public void updateDetails(@RequestParam String name, @RequestParam String number ){
        phoneBookService.updateDetails(name, number);
    }

    @DeleteMapping(path = "delete")
    public void deleteEntry(@RequestParam(name = "name") String name){
        phoneBookService.deleteEntry(name);
    }
}
