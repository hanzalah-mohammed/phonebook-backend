package project.phonebook.dao;

import org.springframework.stereotype.Component;
import project.phonebook.model.PhoneBook;

import java.util.HashMap;
import java.util.Map;

@Component
public class PhoneBookDao {

    // create empty hashmap
    Map<String, String> phoneBook = new HashMap<>();

    // sample entry which will be invoked by the commandLineRunner
    public void addSampleEntries() {
        phoneBook.put("Alex", "89125432");
        phoneBook.put("James", "91230876");
        phoneBook.put("Sally", "87651234");
    }

    // return all entries
    public Map<String, String> getAllEntries(){
        return phoneBook;
    }

    // add new entry
    public void addEntry(PhoneBook entry) {
        boolean exists = phoneBook.containsKey(entry.getName());

        if(exists){
            throw new IllegalStateException("Name already exists!!");
        }

        phoneBook.put(entry.getName(), entry.getNumber());
    }

    public void updateDetails(String name, String number) {
        boolean exists = phoneBook.containsKey(name);

        if(!exists){
            throw new IllegalStateException(name + " is not valid");
        }

        phoneBook.put(name, number);
    }

    public void deleteEntry(String name) {
        boolean exists = phoneBook.containsKey(name);

        if(!exists){
            throw new IllegalStateException(name + " is not valid");
        }

        phoneBook.remove(name);
    }
}
