package project.phonebook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import project.phonebook.dao.PhoneBookDao;
import project.phonebook.model.PhoneBook;

import java.util.Map;

@Service
public class PhoneBookService {

    @Autowired
    private PhoneBookDao phoneBookDao;

    public Map<String, String> getAllEntries() {
        return phoneBookDao.getAllEntries();
    }

    public void addEntry(PhoneBook entry) {
        phoneBookDao.addEntry(entry);
    }

    public void updateDetails(String name, String number) {
        phoneBookDao.updateDetails(name, number);
    }

    public void deleteEntry(String name) {
        phoneBookDao.deleteEntry(name);
    }
}
