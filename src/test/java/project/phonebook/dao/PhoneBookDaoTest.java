package project.phonebook.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

class PhoneBookDaoTest {

    @Autowired
    PhoneBookDao phoneBookDao;

    @Test
    void itShouldAddSampleEntries() {
        // Given
        // When
        phoneBookDao.addSampleEntries();
        // Then
        assertThat(phoneBookDao.phoneBook).isNotEmpty().hasSize(3);
    }

    @Test
    void itShouldGetAllEntries() {
        // Given
        // When
        // Then

    }

    @Test
    void itShouldAddEntry() {
        // Given
        // When
        // Then

    }

    @Test
    void itShouldUpdateDetails() {
        // Given
        // When
        // Then

    }

    @Test
    void itShouldDeleteEntry() {
        // Given
        // When
        // Then

    }
}